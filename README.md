# Avris Backup

A simple Bash backup script that:

 - dumps MySQL databases to `.sql.gz` files
 - compresses specified files (configs, web apps, db dumps, ...) with specified exceptions (cache, ...) into a single archive
 - cleans up older dumps
 - uploads the current dump to S3

## Installation

    cp .env.dist .env
    nano .env        # set up env vars
    nano backup.sh   # adjust to your needs

## Usage

    ./backup.sh

## Copyright

* **Author:** Andrea Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
