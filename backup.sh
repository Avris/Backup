#!/bin/bash

### PREPARE

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DATE=`eval date +%Y%m%d`
source $DIR/.env

containsElement () {
  local e
  for e in "${@:2}"; do [[ "$e" == "$1" ]] && return 0; done
  return 1
}

ensureDir () {
	[ -e $1 ] && rm -r $1
	mkdir -p $1
}

MYSQL="$(which mysql)"
MYSQLDUMP="$(which mysqldump)"
GZIP="$(which gzip)"
TAR="$(which tar)"

### DATABASE

DB_IGNORED=("information_schema" "mysql" "performance_schema" "sys")

DB_TMP_DIR=$DIR/database/$DATE
ensureDir $DB_TMP_DIR

DBS="$($MYSQL -u $DB_USERNAME -h $DB_HOST --password=$DB_PASSWORD -Bse 'show databases')"
for DB in $DBS; do
    if ! containsElement "$DB" "${DB_IGNORED[@]}"; then
        echo "Database $DB"
        $MYSQLDUMP -u $DB_USERNAME -h $DB_HOST --password=$DB_PASSWORD --skip-lock-tables $DB | $GZIP -9 > $DB_TMP_DIR/$DB.sql.gz
    fi
done

### WWW

WWW_DIR="/home/andre/www"
OUTDIR="$DIR/snapshots"
ensureDir $OUTDIR

FILE="$OUTDIR/$DATE.tar.gz"
[ -e $FILE ] && rm $FILE
echo "OUTPUT FILE $FILE"

tar -zcf $FILE \
    --exclude="**/vendor/*" \
    --exclude="**/.git/*" \
    --exclude="**/app/cache/*" \
    --exclude="**/app/logs/*" \
    --exclude="**/web/assetic/*" \
    --exclude="**/web/imagine/*" \
    --exclude="**/run/cache/*" \
    --exclude="**/run/logs/*" \
    --exclude="**/var/logs/*" \
    --exclude="**/var/cache/*" \
    --exclude="**/node_modules*" \
    --exclude="**/media/cache/*" \
    --exclude="**/matomo/*" \
    --exclude="**/release/*" \
    --exclude="**/releases/*" \
    --exclude="**/current/*" \
    --exclude="**/var/spool/*" \
    --verbose \
    $WWW_DIR \
    $DB_TMP_DIR \
    /etc/apache2/sites-available \
    /etc/apache2/sites-enabled \
    /etc/supervisor \
    /var/spool/cron/crontabs \
    $DIR/build-backup.sh \
    $DIR/.env \
    $WWW_DIR/../monitor.sh

chmod 755 $FILE

### CLEAN

echo "Cleaning up"

rm -r $DB_TMP_DIR

find $OUTDIR -mtime +3 -delete

### UPLOAD

# http://tmont.com/blargh/2014/1/uploading-to-s3-in-bash

RESOURCE="server/$DATE.tar.gz"
S3_DATE=`date -R`
SIGNATURE=`echo -en "PUT\n\napplication/x-compressed-tar\n${S3_DATE}\n/$AWS_S3_BUCKET/$RESOURCE" | openssl sha1 -hmac $AWS_SECRET -binary | base64`
S3_TARGET="https://$AWS_S3_BUCKET.s3-$AWS_REGION.amazonaws.com/$RESOURCE"

echo "Uploading to $S3_TARGET"

curl -X PUT -T "$FILE" \
  -H "Host: $AWS_S3_BUCKET.s3.amazonaws.com" \
  -H "Date: ${S3_DATE}" \
  -H "Content-Type: application/x-compressed-tar" \
  -H "Authorization: AWS $AWS_KEY:${SIGNATURE}" \
  $S3_TARGET
